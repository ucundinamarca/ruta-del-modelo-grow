/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "both",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'fondo',
                            type: 'image',
                            rect: ['0px', '0px', '1023px', '637px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Asset_99.png",'0px','0px']
                        },
                        {
                            id: 'stage1',
                            type: 'group',
                            rect: ['9', '0', '1016', '641', 'auto', 'auto'],
                            c: [
                            {
                                id: 'foto',
                                type: 'image',
                                rect: ['372px', '0px', '644px', '641px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Asset_97.png",'0px','0px']
                            },
                            {
                                id: 'title',
                                type: 'image',
                                rect: ['0px', '53px', '585px', '260px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Asset_98.png",'0px','0px']
                            },
                            {
                                id: 'btn_play',
                                type: 'image',
                                rect: ['173px', '385px', '155px', '156px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Asset_77.png",'0px','0px'],
                                userClass: "pointer"
                            }]
                        },
                        {
                            id: 'container_slider',
                            type: 'group',
                            rect: ['0', '0', '1025', '641', 'auto', 'auto'],
                            c: [
                            {
                                id: 'slider',
                                type: 'group',
                                rect: ['0', '0', '1025', '641', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'slider_1',
                                    type: 'group',
                                    rect: ['18', '11', '991', '575', 'auto', 'auto'],
                                    userClass: "sliders",
                                    c: [
                                    {
                                        id: 'btn-icon-1',
                                        type: 'group',
                                        rect: ['59', '207', '173', '251', 'auto', 'auto'],
                                        userClass: "pointer btn",
                                        c: [
                                        {
                                            id: 'label-1-1',
                                            type: 'image',
                                            rect: ['0px', '201px', '165px', '50px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Asset_88.png",'0px','0px']
                                        },
                                        {
                                            id: 'icon-1-1',
                                            type: 'image',
                                            rect: ['0px', '0px', '173px', '173px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Asset_84.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'btn-icon-2',
                                        type: 'group',
                                        rect: ['328', '207', '170', '250', 'auto', 'auto'],
                                        userClass: "pointer btn",
                                        c: [
                                        {
                                            id: 'label-1-2',
                                            type: 'image',
                                            rect: ['0px', '202px', '165px', '48px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Asset_89.png",'0px','0px']
                                        },
                                        {
                                            id: 'icon-1-2',
                                            type: 'image',
                                            rect: ['0px', '0px', '170px', '176px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Asset_85.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'btn-icon-3',
                                        type: 'group',
                                        rect: ['558', '207', '183', '251', 'auto', 'auto'],
                                        userClass: "pointer btn",
                                        c: [
                                        {
                                            id: 'label-1-3',
                                            type: 'image',
                                            rect: ['18px', '202px', '165px', '49px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Asset_90.png",'0px','0px']
                                        },
                                        {
                                            id: 'icon-1-3',
                                            type: 'image',
                                            rect: ['0px', '0px', '173px', '172px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Asset_86.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'btn-icon-4',
                                        type: 'group',
                                        rect: ['811', '207', '172', '251', 'auto', 'auto'],
                                        userClass: "pointer btn",
                                        c: [
                                        {
                                            id: 'label-1-4',
                                            type: 'image',
                                            rect: ['0px', '201px', '165px', '50px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Asset_91.png",'0px','0px']
                                        },
                                        {
                                            id: 'icon-1-4',
                                            type: 'image',
                                            rect: ['0px', '0px', '172px', '180px', 'auto', 'auto'],
                                            fill: ["rgba(0,0,0,0)",im+"Asset_87.png",'0px','0px']
                                        }]
                                    },
                                    {
                                        id: 'flecha1',
                                        type: 'image',
                                        rect: ['124px', '146px', '243px', '53px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Asset_82.png",'0px','0px']
                                    },
                                    {
                                        id: 'flecha2',
                                        type: 'image',
                                        rect: ['399px', '488px', '245px', '43px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Asset_83.png",'0px','0px']
                                    },
                                    {
                                        id: 'flecha3',
                                        type: 'image',
                                        rect: ['670px', '140px', '243px', '53px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Asset_82.png",'0px','0px']
                                    },
                                    {
                                        id: 'icon',
                                        type: 'image',
                                        rect: ['72px', '98px', '57px', '57px', 'auto', 'auto'],
                                        cursor: 'pointer',
                                        fill: ["rgba(0,0,0,0)",im+"icon.svg",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'slider_2',
                                    type: 'rect',
                                    rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,0.00)"],
                                    stroke: [0,"rgba(0,0,0,1)","none"],
                                    userClass: "sliders",
                                    c: [
                                    {
                                        id: 't2',
                                        type: 'rect',
                                        rect: ['89px', '170px', '852px', '324px', 'auto', 'auto'],
                                        fill: ["rgba(255,255,255,0.00)"],
                                        stroke: [0,"rgba(0,0,0,1)","none"],
                                        userClass: ""
                                    }]
                                }]
                            },
                            {
                                id: 'btn_credit',
                                type: 'image',
                                rect: ['28px', '16px', '59px', '73px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Asset_81.png",'0px','0px'],
                                userClass: "pointer"
                            },
                            {
                                id: 'btn_home',
                                type: 'image',
                                rect: ['951px', '16px', '58px', '73px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Asset_80.png",'0px','0px'],
                                userClass: "pointer"
                            },
                            {
                                id: 'btn_next',
                                type: 'image',
                                rect: ['956px', '580px', '47px', '46px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Asset_95.png",'0px','0px'],
                                userClass: "pointer"
                            },
                            {
                                id: 'btn_prev',
                                type: 'image',
                                rect: ['24px', '580px', '47px', '46px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Asset_96.png",'0px','0px'],
                                userClass: "pointer"
                            }]
                        },
                        {
                            id: 'alert_tip',
                            type: 'group',
                            rect: ['-2', '4', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'alerta',
                                type: 'image',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"alerta.png",'0px','0px']
                            },
                            {
                                id: 'close_tip',
                                type: 'image',
                                rect: ['947px', '18px', '56px', '56px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Asset_94.png",'0px','0px'],
                                userClass: "pointer"
                            }]
                        },
                        {
                            id: 'alerts',
                            type: 'group',
                            rect: ['-20px', '0px', '1046', '641', 'auto', 'auto'],
                            c: [
                            {
                                id: 'background',
                                type: 'image',
                                rect: ['20px', '0px', '1025px', '641px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Asset_92.png",'0px','0px']
                            },
                            {
                                id: 'container_box',
                                type: 'group',
                                rect: ['59', '43px', '962', '561', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'box',
                                    type: 'image',
                                    rect: ['0px', '0px', '946px', '561px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Asset_93.png",'0px','0px']
                                },
                                {
                                    id: 'legend',
                                    type: 'rect',
                                    rect: ['33px', '328px', '241px', '100px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'text',
                                    type: 'rect',
                                    rect: ['337px', '56px', '563px', '454px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'icon1',
                                    type: 'group',
                                    rect: ['73px', '70px', '173', '251', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'label-2-1',
                                        type: 'image',
                                        rect: ['0px', '201px', '165px', '50px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Asset_88.png",'0px','0px']
                                    },
                                    {
                                        id: 'icon-2-1',
                                        type: 'image',
                                        rect: ['0px', '0px', '173px', '173px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Asset_84.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'icon2',
                                    type: 'group',
                                    rect: ['73px', '70px', '170', '250', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'label-2-2',
                                        type: 'image',
                                        rect: ['0px', '202px', '165px', '48px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Asset_89.png",'0px','0px']
                                    },
                                    {
                                        id: 'icon-2-2',
                                        type: 'image',
                                        rect: ['0px', '0px', '170px', '176px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Asset_85.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'icon3',
                                    type: 'group',
                                    rect: ['73px', '70px', '173', '251', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'label-2-3',
                                        type: 'image',
                                        rect: ['0px', '202px', '165px', '49px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Asset_90.png",'0px','0px']
                                    },
                                    {
                                        id: 'icon-2-3',
                                        type: 'image',
                                        rect: ['0px', '0px', '173px', '172px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Asset_86.png",'0px','0px']
                                    }]
                                },
                                {
                                    id: 'icon4',
                                    type: 'group',
                                    rect: ['73px', '70px', '172', '251', 'auto', 'auto'],
                                    c: [
                                    {
                                        id: 'label-2-4',
                                        type: 'image',
                                        rect: ['0px', '201px', '165px', '50px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Asset_91.png",'0px','0px']
                                    },
                                    {
                                        id: 'icon-2-4',
                                        type: 'image',
                                        rect: ['0px', '0px', '172px', '180px', 'auto', 'auto'],
                                        fill: ["rgba(0,0,0,0)",im+"Asset_87.png",'0px','0px']
                                    }]
                                }]
                            },
                            {
                                id: 'close',
                                type: 'image',
                                rect: ['965px', '22px', '56px', '56px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Asset_94.png",'0px','0px'],
                                userClass: "pointer"
                            }]
                        },
                        {
                            id: 'credits',
                            type: 'group',
                            rect: ['0', '0', '1023', '637', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle4',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'text_credits',
                                type: 'rect',
                                rect: ['276px', '116px', '410px', '503px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'fondo2',
                                type: 'image',
                                rect: ['0px', '0px', '1023px', '637px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Asset_99.png",'0px','0px']
                            },
                            {
                                id: 'Asset_101',
                                type: 'image',
                                rect: ['729px', '310px', '252px', '292px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Asset_101.png",'0px','0px']
                            },
                            {
                                id: 'Asset_100',
                                type: 'image',
                                rect: ['35px', '35px', '291px', '85px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Asset_100.png",'0px','0px']
                            },
                            {
                                id: 'btn_home2',
                                type: 'image',
                                rect: ['951px', '11px', '58px', '73px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Asset_80.png",'0px','0px'],
                                userClass: "pointer"
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '640', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-preload',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '640px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '270px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '640px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [

                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-247297");

var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Guia General",
    autor: "Edilson Laverde Molina",
    date: "",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios = [{
    url: "sonidos/click.mp3",
    name: "clic"
}];

html = [
    {
        id: ST+'t1',
        type:1,
        html:`
        <p>
            Por favor siga el paso a paso:
        </p>
        <p>
            Consulte el recurso educativo que se presenta a continuación 
            Haga clic en los puntos de información o links que encuentre a medida avanza en el recurso.
            Realice las actividades propuestas
        </p>
        ` 
    },
    {
        id: ST+'t2',
        type:1,
        html:`
        <div class="pop">
        <p>
        El modelo Grow es una herramienta para destacar, obtener y maximizar la confianza interior, mediante una serie de conversaciones de coaching.​
        </p>
        <p>
        Su eficacia trasciende los límites de la disciplina, la cultura y la personalidad para lograr metas, objetivos y propósitos de forma estructurada, permitiendo realizar el seguimiento a las actividades plasmadas. 

        </p>
        </div>
        `
    },
    {
        id: ST+'text_credits',
        type:1,
        html: `
        <div class="credit">
            <p>
                <div><b>Nombre del material</b></div>
                <div>Coaching</div>
            </p>

            <p>
                <div><b>Experto Temático</b></div>
                <div>Ruth Arroyo Tovar, María José Angulo</div>
            </p>

            <p>
                <div><b>Diseñador Instruccional</b></div>
                <div>Laura Victoria Gómez R, Yishad Hugo Yérmanos A.</div>
            </p>
            <p>
                <div><b>Diseño Gráfico</b></div>
                <div>Andrés Guillermo Martínez G.</div>
            </p>
            <p>
                <div><b>Programador</b></div>
                <div>Edilson Laverde Molina</div>
            </p>
            <p>
                <div><b>Diseñador Multimedia</b></div>
                <div>Luis Francisco Sierra, Ginés Velásquez</div>
            </p>
            <p>
                <div><b>Imágenes e iconos</b></div>
                <div>Freepik, Flaticon, Shutterstock, Pexels</div>
            </p>
            <p>
                <div><b>Oficina de Educación Virtual y a Distancia</b></div>
                <div>Universidad de Cundinamarca</div>
                <div>2023</div>
            </p>
        </div>
        `
    },
    {
        target: ST+'btn-icon-1',
        type: 2,
        html1:`
        <p  class="pop n">
        Comience por definir una meta específica cumpliendo con los principios mencionados.
        </p>
        `,
        html2:`
        <div  class="pop">
            <h1>
                Goal – Meta
            </h1> 
            <p>
                ¿Qué es lo que se quiere?
                </p>
            <p>
            Es importante que formule sus objetivos en infinitivo, de forma asertiva y cumpliendo con las características conocidas como <b>M.A.R.T.E. o S.M.A.R.T</b> en ingles (<b>M</b>edibles, <b>A</b>lcanzables, <b>R</b>elevantes, definidos en el <b>T</b>iempo y <b>E</b>specíficos).
            </p>
        </div>
        `, 
    },
    {
        target: ST+'btn-icon-2',
        type: 2,
        html1:`
        <p  class="pop n">
            Cuando reconoce su problema teniendo en cuenta distintos puntos de vista, es capaz de identificar más de una solución.
        </p>
        `,
        html2:`
        <div  class="pop">
            <h1>Reality – Realidad</h1> 
            <p>¿Dónde está ahora? ¿Cuáles son sus posibilidades en el momento?</p>
            <p>De forma objetiva e imparcial, procure analizar su realidad actual, de este modo podrá ser consciente de cuán cerca o lejos se encuentra de alcanzar su(s) objetivo(s).​</p>
            <p>Indague sobre sus acciones pasadas y plantéese alternativas para alcanzar un mejor rendimiento, identifique sus recursos y fuentes de ayuda a disposición, además de los obstáculos que frenan su avance en el momento.​</p>
        </div>
        `, 
    },
    {
        target: ST+'btn-icon-3',
        type: 2,
        html1:`
        <p  class="pop n">
            Amplie su perspectiva y aumente su creatividad en la búsqueda de posibles opciones.
        </p>
        `,
        html2:`
        <div  class="pop">
            <h1>Options – Opciones</h1> 
            <p>¿Qué puedo hacer?  ¿Cómo puedo ejecutar la opción escogida?</p>
            <p>Elabore una lista de posibles opciones, que le pueda contribuir en el alcance de sus objetivos, debe contener todas opciones y posibilidades contempladas, es importante que incluya también las limitaciones de cada opción. El coach le apoyará con más preguntas que permitan superar dichas limitaciones.  ​
            </p>
        </div>
        `, 
    },
    {
        target: ST+'btn-icon-4',
        type: 2,
        html1:`
        <p class="pop n">
        Tome una decisión adecuada y defina su ruta de acción, justifique su actuar sobre el por qué debe hacerse así. 
        </p>
        `,
        html2:`
        <div  class="pop">
            <h1>Will– Voluntad</h1> 
            <p>¿Qué haré de ahora en adelante?, ¿cuáles son los obstáculos que tengo que superar?, ¿cuáles son mis probabilidades de éxito?</p>
            <p>Defina y siga el plan de acción orientado su objetivo, estimule su automotivación y establezca un compromiso con sí mismo en la consecución de su objetivo.</p>
        </div>
        `, 
    },
]
function main(sym) {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            t.events();
            t.animations();
            t.setHTMLType1();
            //precarga audios//
            ivo.load_audio(audios, onComplete = function () {
                ivo(ST + "preload").hide();
                stage1.play();
            });
        },
        methods: {
            setHTMLType1: function () {
                for (var i = 0; i < html.length; i++) {
                    if (html[i].type == 1) {
                        ivo(html[i].id).html(html[i].html);
                    }
                }
            },

            events: function () {
                var t = this;

                      //======================================== Controlador de slides ========================================//
                //configuración slider principal//
                var slider=ivo(ST+"slider").slider({
                    slides:'.sliders',
                    btn_next:ST+"btn_next",
                    btn_back:ST+"btn_prev",
                    rules:function(rule){
                        console.log("rules"+rule);
                    },
                    onMove:function(page){
                        console.log("onMove"+page);
                        ivo.play("clic");
                    },
                    onFinish:function(){
                    }
                });
                ivo(ST+"icon").on("click", function () {
                    ivo.play("clic");
                    alert_tip.timeScale(1).play();
                });
                ivo(ST+"close_tip").on("click", function () {
                    ivo.play("clic");
                    alert_tip.timeScale(3).reverse();
                });

                ivo(ST+"btn_play").on("click", function () {
                    stage1.timeScale(5).reverse();
                    ivo.play("clic");
                    container_slider.timeScale(1).play();
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });
                ivo(ST+"btn_credit").on("click", function () {
                    credits.timeScale(1).play();
                    ivo.play("clic");
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });
                ivo(ST+"close").on("click", function () {
                    alerts.timeScale(2).reverse();
                    ivo.play("clic");
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });
                ivo(ST+"btn_home").on("click", function () {
                    stage1.timeScale(1).play();
                    container_slider.timeScale(3).reverse();
                    ivo.play("clic");
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });
                ivo(ST+"btn_home2").on("click", function () {
                    credits.timeScale(3).reverse();
                    ivo.play("clic");
                })
                .on("mouseover", function () {

                })
                .on("mouseout", function () {
                    
                });
                ivo(".btn").on("click", function () {
                    ivo.play("clic");
                    let id = ivo(this).attr("id");
                    let n = ivo(this).attr("id").split(S+"btn-icon-")[1];
                    $(ST+"icon1,"+ST+"icon2,"+ST+"icon3,"+ST+"icon4").hide();

                    $(ST+"icon"+n).show();
                    for (var i = 0; i < html.length; i++) {
                        if (html[i].target == "#"+id) {
                            ivo(ST+"legend").html(html[i].html1);
                            ivo(ST+"text").html(html[i].html2);
                            alerts.timeScale(1).play();
                        }
                    }
                });

            },
            animations: function () {
                stage1 = new TimelineMax({
                    onComplete: function () {
                        $(ST+"btn_play").addClass("animated infinite pulse");
                    }
                });
                stage1.append(TweenMax.from(ST + "stage1", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "title", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "foto", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "btn_play", .8, {scaleY: 1300, opacity: 0,rotation:900}), 0);
                //stage1.append(TweenMax.staggerFrom(".example", .4, {x: 100, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage1.stop();

                credits = new TimelineMax();
                credits.append(TweenMax.from(ST + "credits", .8, {x: 1300, opacity: 0}), 0);
                credits.append(TweenMax.from(ST + "btn_home2", .8, {scaleX: 4, opacity: 0,rotation:900}), 0);
                credits.append(TweenMax.from(ST + "Asset_100", .8, {x: -300, opacity: 0}), 0);
                credits.append(TweenMax.from(ST + "Asset_101", .8, { opacity: 0}), -.6);
                credits.append(TweenMax.from(ST + "text_credits", .8, { opacity: 0}), -.4);
                credits.stop();

                alerts = new TimelineMax();
                alerts.append(TweenMax.from(ST + "alerts", .8, {x: 1300, opacity: 0}), 0);
                alerts.append(TweenMax.from(ST + "container_box", .8, {y: 1300, opacity: 0}), 0);
                alerts.append(TweenMax.from(ST + "close", .8, {scaleX: 4, opacity: 0,rotation:900}), 0);
                alerts.stop();
                
                alert_tip = new TimelineMax();
                alert_tip.append(TweenMax.from(ST + "alert_tip", .8, {x: 1300, opacity: 0}), 0);
                alert_tip.append(TweenMax.from(ST + "close_tip", .8, {scaleX: 4, opacity: 0,rotation:900}), 0);
                alert_tip.stop();

                container_slider = new TimelineMax();
                container_slider.append(TweenMax.from(ST + "container_slider", .8, {y: 1300, opacity: 0}), 0);
                container_slider.stop();
            }
        }
    });
}